let fs = require("fs");

let checkFileJSON = (FileName) => {
    return new Promise((resolve, reject) => {
        fs.exists(FileName , (exists) => {
            if(exists) {
                resolve(exists)
            } else {
                reject("error")
            }
        })
    })
}

let readFileJS = (FileName) => {
  return new Promise((resolve, reject) => {
    fs.readFile(FileName, "utf8", (err, data) => {
      if (err) 
        reject(err);
      else 
        resolve(JSON.parse(data));
    });
  });
};

let TestFun = async () => {
    let cf = await checkFileJSON("homework5-1_eyes.json")
    console.log(cf)
}

TestFun()

exports.readFileJS = readFileJS
exports.checkFileJSON = checkFileJSON

// console.log(readFileJS("homework5-1_eyes.json"));
