let fs = require("fs")
let jsFun = require("./jsFun.js")

fs.readFile('homework5_1.json', 'utf8', (err, data) => {
    let jsonData = JSON.parse(data)

    let obj = {}
    let brown =0, blue = 0, green = 0
    for (const key in jsonData) {
      
        if(jsonData[key].eyeColor == "brown") {
            brown++
        }
        if(jsonData[key].eyeColor == "green") {
            green++
        }
        if(jsonData[key].eyeColor == "blue") {
            blue++
        }
    
    }
    obj.brown = brown
    obj.green = green
    obj.blue = blue

    fs.writeFile('homework5-1_eyes.json', JSON.stringify(obj) , 'utf8', (err) => {
        if(err) {
            console.log(err)
        } else {
            console.log("write homework5-1_eyes success")
        }
    })
})

let createdEyeJson = async () => {
    const readJson = await jsFun.readFileJS('homework5_1.json')

    let readFilter = readJson.map((data) => {
        let obj = {}
        obj[data.gender] = data.eyeColor
        return obj
    })

    let readReduce = readFilter.reduce((allColor, Color) => {
        // if(Object.keys(Color).male === 'male') {
        //     allColor[Color.male]++
        // } else {
        //     allColor[Color.male] = 1
        // }
        console.log(Object.values(Color).hasOwnProperty('blue'))
        obj = {}
        if(Color.hasOwnProperty('male')) {
            if(Object.values(Color).hasOwnProperty('blue')) {
                obj[Object.values(Color)] = "blue"
            }
            // obj[Object.values(Color)] = obj[Object.values(Color)]++
            // obj[Object.values(Color)] = obj[Object.values(Color)]++
        } 

        if(Color.hasOwnProperty('female')) {

            // obj[Object.values(Color)] = obj[Object.values(Color)]++
            if(Object.values(Color) === 'blue') {
                obj[Object.values(Color)] = "blue"
            }
        } 
        
        allColor[Object.keys(Color)] = obj
        return allColor
    },{})
    
    console.log(readReduce)

   

    // let countEyeColor = readJson.reduce((total, data) => {
    //     total[data.eyeColor] = (data.eyeColor === 'brown' || 0) +1
    //     console.log(total)
    //     return total
    // },{})
}

console.log(createdEyeJson())

fs.readFile('homework5_1.json', 'utf8', (err, data) => {
    let jsonData = JSON.parse(data)

    let obj = {}
    let male =0, female = 0
    for (const key in jsonData) {
      
        if(jsonData[key].gender == "male") {
            male++
        }
        if(jsonData[key].gender == "female") {
            female++
        }
    }
    obj.male = male
    obj.female = female

    fs.writeFile('homework5- 1_gender.json', JSON.stringify(obj) , 'utf8', (err) => {
        if(err) {
            console.log(err)
        } else {
            console.log("write homework5- 1_gender success")
        }
    })
})

fs.readFile('homework5_1.json', 'utf8', (err, data) => {
    let jsonData = JSON.parse(data)

    let arr = []
    let male =0, female = 0
    for (const key in jsonData) {
        let obj = {}
        if(jsonData[key].gender == "male") {
            male++
        }
        if(jsonData[key].gender == "female") {
            female++
        }


        obj.id = jsonData[key]._id
        obj.friendCount = jsonData[key].friends.length
        arr.push(obj)
    }

    fs.writeFile('homework5- 1_friends.json', JSON.stringify(arr) , 'utf8', (err) => {
        if(err) {
            console.log(err)
        } else {
            console.log("write homework5_1 success")
        }
    })
})

