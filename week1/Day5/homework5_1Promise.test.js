let fs = require("fs")
let assert = require('assert')
let dataTest = require('./jsonTest.js')
let jsFun = require('./jsFun.js')

describe("check file", () => {
    describe("#checkFileExist()", () => {

        it("should have homework5-1_eyes.json existed", () => {
            let checkFile = fs.existsSync('homework5-1_eyes.json')
            assert.equal(checkFile, true)
        })

        it("should have homework5-1_gender.json existed", () => {

            let checkFile = fs.existsSync('homework5- 1_gender.json')
            assert.equal(checkFile, true)
        })

        it("should have homework5-1_friends.json existed", () => {
            let checkFile = fs.existsSync('homework5- 1_friends.json')
            assert.equal(checkFile, true)
        })

    }) 

    describe("#ObjectKey()", () => {
        it('should have same object key stucture as homework5-1_eyes.json', async() => {
            const data = await jsFun.readFileJS("homework5-1_eyes.json")
            assert.deepEqual(Object.keys(data), dataTest.keyEyeColor() )
        });

        it('should have same object key stucture as homework5- 1_gender.json', async() => {
            const data = await jsFun.readFileJS("homework5- 1_gender.json")
            assert.deepEqual(Object.keys(data), dataTest.keyGender() )
        });

        it('should have same object key stucture as homework5- 1_friends.json', async() => {
            const data = await jsFun.readFileJS("homework5- 1_friends.json")
            for (const key in data) {
                assert.deepEqual(Object.keys(data[key]), dataTest.keyFrined() )
            }

        });

    })

    describe('#userFriendCount()', function() {
        it('should have size of array input as 23', async() => {
            const data = await jsFun.readFileJS('homework5- 1_friends.json')
            assert.strictEqual(data.length, 23)
        });
    });

    describe('#sumOfEyes()', function() {
        it('should have sum of eyes as 23', async() => {
            const data = await jsFun.readFileJS('homework5-1_eyes.json')
            let totalEyes = Object.values(data).reduce((total, data) => total + data, 0);
            assert.strictEqual(totalEyes, 23)
        });
    });

    describe('#sumOfGender()', function() {
        it('should have sum of gender as 23', async() =>  {
            const data = await jsFun.readFileJS('homework5- 1_gender.json')
            let totalGender = Object.values(data).reduce((total, data) => total + data, 0)
            assert.strictEqual(totalGender, 23)
        });
    });
    
})
