let keyEyeColor = () => {
    return ['brown', 'green', 'blue']
}

let keyGender = () => {
    return ['male', 'female']
}

let keyFrined = () => {
    return ['id', 'friendCount']
}

exports.keyEyeColor = keyEyeColor
exports.keyGender = keyGender
exports.keyFrined = keyFrined
