let fs = require("fs")
let assert = require('assert')
let jsonTest = require('./jsonTest.js')

describe("check file", () => {
    describe("#checkFileExist()", () => {

        it("should have homework5-1_eyes.json existed", () => {
            let checkFile = fs.existsSync('homework5-1_eyes.json')
            assert.equal(checkFile, true)
        })

        it("should have homework5-1_gender.json existed", () => {

            let checkFile = fs.existsSync('homework5- 1_gender.json')
            assert.equal(checkFile, true)
        })

        it("should have homework5-1_friends.json existed", () => {
            let checkFile = fs.existsSync('homework5- 1_friends.json')
            assert.equal(checkFile, true)
        })

    }) 

    describe("#ObjectKey()", () => {
        it('should have same object key stucture as homework5-1_eyes.json', function(done) {
            fs.readFile('homework5-1_eyes.json', 'utf8', (err, data) => {
                if (err) {
                    done(err)
                }
                const eyesKeys = ['brown', 'green', 'blue']
                let jsonData = JSON.parse(data)
                assert.deepEqual(Object.keys(jsonData), jsonTest.keyEyeColor() )
                done()
            })
        });

        it('should have same object key stucture as homework5- 1_gender.json', function(done) {
            fs.readFile('homework5- 1_gender.json', 'utf8', (err, data) => {
                if (err) {
                    done(err)
                }
                const genderKey = ['male', 'female']
                let jsonData = JSON.parse(data)
                assert.deepEqual(Object.keys(jsonData), genderKey )
                done()
            })
        });

        it('should have same object key stucture as homework5- 1_friends.json', function(done) {
            fs.readFile('homework5- 1_friends.json', 'utf8', (err, data) => {
                if (err) {
                    done(err)
                }
                let friendKey = ['id', 'friendCount']
                let jsonData = JSON.parse(data)

                for (const key in jsonData) {
                    assert.deepEqual(Object.keys(jsonData[key]), friendKey )
                }
                done()
            })
        });

    })

    describe('#userFriendCount()', function() {
        it('should have size of array input as 23', function(done) {
            fs.readFile('homework5- 1_friends.json', 'utf8', (err, data) => {
                if (err) {
                    done(err)
                }
                let jsonData = JSON.parse(data)
                assert.strictEqual(jsonData.length, 23)
                done()
            })
        });
    });

    describe('#sumOfEyes()', function() {
        it('should have sum of eyes as 23', function(done) {
            fs.readFile('homework5-1_eyes.json', 'utf8', (err, data) => {
                if (err) {
                    done(err)
                }
                let jsonData = JSON.parse(data)
                let totalEyes = Object.values(jsonData).reduce((total, data) => total + data, 0);
                assert.strictEqual(totalEyes, 23)
                done()
            })
        });
    });

    describe('#sumOfGender()', function() {
        it('should have sum of gender as 23', function(done) {
            fs.readFile('homework5- 1_gender.json', 'utf8', (err, data) => {
                if (err) {
                    done(err)
                }
                let jsonData = JSON.parse(data)
                let totalGender = Object.values(jsonData).reduce((total, data) => total + data, 0)
                assert.strictEqual(totalGender, 23)
                done()
            })
        });
    });
    
})
