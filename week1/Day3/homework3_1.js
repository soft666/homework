let fs = require('fs')

let p1 = new Promise(function (resolve, reject) {
    fs.readFile('head.txt', 'utf8', function (err, data) {
            if (err)
                reject(err);
            else
                resolve(data);
        });
});

let p2 = new Promise(function (resolve, reject) {
    fs.readFile('feet.txt', 'utf8', function (err, data) {
            if (err)
                reject(err);
            else
                resolve('\n'+data);
        });
});

let p3 = new Promise(function (resolve, reject) {
    fs.readFile('body.txt', 'utf8', function (err, data) {
            if (err)
                reject(err);
            else
                resolve('\n'+data);
        });
});

let p4 = new Promise(function (resolve, reject) {
    fs.readFile('leg.txt', 'utf8', function (err, data) {
            if (err)
                reject(err);
            else
                resolve('\n'+data);
        });
});

function writeP(data) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('robot.txt', data, 'utf8'
            , function (err) {
                if (err)
                    reject(err);
                else
                    resolve("Write Success!");
            });
    });
}

Promise.all([p1, p2, p3, p4])
    .then(function (result) {
        console.log('read completed!: ', result.join(""));
        return writeP(result.join(""))
    })
    .then(function (result){
        console.log(result)
    })
    .catch(function (error) {
        console.error("There's an error", error);
    });