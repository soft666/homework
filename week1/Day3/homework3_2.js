let fs = require('fs')

let p1 = new Promise(function (resolve, reject) {
    fs.readFile('head.txt', 'utf8', function (err, data) {
        if (err)
            reject(err);
        else
            resolve(data);
    });
});

let p2 = new Promise(function (resolve, reject) {
    fs.readFile('feet.txt', 'utf8', function (err, data) {
        if (err)
            reject(err);
        else
            resolve('\n' + data);
    });
});

let p3 = new Promise(function (resolve, reject) {
    fs.readFile('body.txt', 'utf8', function (err, data) {
        if (err)
            reject(err);
        else
            resolve('\n' + data);
    });
});

let p4 = new Promise(function (resolve, reject) {
    fs.readFile('leg.txt', 'utf8', function (err, data) {
        if (err)
            reject(err);
        else
            resolve('\n' + data);
    });
});

function writeP(data) {
    return new Promise(function (resolve, reject) {
        fs.writeFile('robot.txt', data, 'utf8'
            , function (err) {
                if (err)
                    reject(err);
                else
                    resolve("Write Success!");
            });
    });
}

async function WriteFile() {
    try {
        let head = await p1
        let feet = await p2
        let body = await p3
        let leg = await p4
        return writeP(head+feet+body+leg)
    } catch (error) {
        console.error(error);
    }
}
WriteFile();