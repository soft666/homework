let fs = require('fs')

let employee =
[
    {"id":"1001",'firstname':'Luke','lastname':'Skywalker'},
    {"id":"1002",'firstname':'Tony','lastname':'Stark'},
    {"id":"1003",'firstname':'Somchai','lastname':'Jaidee'},
    {"id":"1004",'firstname':'Monkey D','lastname':'Luffee'},
];
let company = [
    {"id":"1001","company":"Walt Disney"},
    {"id":"1002","company":"Marvel"},
    {"id":"1003","company":"Love2work"},
    {"id":"1004","company":"One Piece"},
];
let salary = [
    {"id":"1001","salary":"40000"},
    {"id":"1002","salary":"1000000"},
    {"id":"1003","salary":"20000"},
    {"id":"1004","salary":"9000000"},
];
let like = [
    {"id":"1001","like":"apple"},
    {"id":"1002","like":"banana"},
    {"id":"1003","like":"orange"},
    {"id":"1004","like":"papaya"},
];
let dislike = [
    {"id":"1001","dislike":"banana"},
    {"id":"1002","dislike":"orange"},
    {"id":"1003","dislike":"papaya"},
    {"id":"1004","dislike":"apple"},
];

let employeesDatabase = []
let arrCompany = []

for (const key_employee in employee) {

    let obj = {}
    
    for (const employee_key in employee[key_employee]) {
        obj[employee_key] = employee[key_employee][employee_key]
    }

    for (const company_key in company[key_employee]) {
        obj[company_key] = company[key_employee][company_key]
    }
    
    for (const salary_key in salary[key_employee]) {
        obj[salary_key] = salary[key_employee][salary_key]
    }

    for (const like_key in like[key_employee]) {
        obj[like_key] = like[key_employee][like_key]
    }

    for (const dislike_key in dislike[key_employee]) {
        obj[dislike_key] = dislike[key_employee][dislike_key]
    }
    
    employeesDatabase[key_employee] = obj
}

fs.writeFile('homework3-3.json', JSON.stringify(employeesDatabase), 'utf8', function(err) {
    if(err) {
        console.log("error")
    } else {
        console.log("success")
    }
})

