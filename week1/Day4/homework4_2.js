let fs = require("fs")

fs.readFile('homework1.json', 'utf8', function(err, data) {
    let jsonData = JSON.parse(data)

    const result = jsonData.map(data => {
        data.fullname = data.firstname+' '+data.lastname
        let salary1 = data.salary
        let salary2 = parseInt(salary1)+(data.salary*0.1)
        let salary3 = parseInt(salary2)+(salary2*0.1)
        data.salary = [salary1, salary2, salary3]
        return data
    })

    console.log(result)
    
})