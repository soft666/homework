let fs = require('fs')

fs.readFile('homework1_4.json', 'utf8', function(err, data) {
    let jsonData = JSON.parse(data)

    const resultFilter = jsonData.filter(data => {
        return data.friends.length >= 2 && data.gender === "male"
    })

    const resultMap = resultFilter.map(data => {
        
        let obj = {}
        obj.name = data.name
        obj.gender = data.gender
        obj.company = data.company
        obj.email = data.email
        obj.friends = data.friends
        let balanceCal = data.balance.substring(1).replace(",","")
        obj.balance = "$"+parseFloat(balanceCal)/10
        return data.name
        
    })

    console.log(resultMap)
})