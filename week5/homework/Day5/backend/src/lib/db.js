const mysql2 = require('mysql2/promise')

module.exports = async (config) => {
    const conn = await mysql2.createPool(config)
    return {
        execute(sql, params = []) {
            return conn.execute(sql, params)
        }
    }
}
