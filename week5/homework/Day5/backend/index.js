const Koa = require('koa')
const Router = require('koa-router')
const bodyparser = require('koa-bodyparser')
const cors = require('@koa/cors')

const config = require('./src/config/db.json')
const Database = require('./src/lib/db.js')



let main = async () => {
    const app = new Koa()

    let db = await Database(config)


    let router = new Router()
    router.put('/add/book', async (ctx) => {
        let { book_isbn, book_name, book_price, book_img, book_pomo } = ctx.request.body

        if((book_isbn), (book_name), (book_price), (book_img), (book_pomo)) {
            await db.execute(`INSERT INTO books(book_isbn, book_name, book_price, book_img, book_pomo) VALUES (
                ?,?,?,?,?)` , [book_isbn, book_name, book_price, book_img, book_pomo])
    
            ctx.body = ctx.request.body
            return
        }
        
    })
    .get('/list/book', async (ctx) => {
        let [result] = await db.execute(`select * from books`)
        ctx.body = result
    })
    .post('/update/book', async (ctx) => {
        let { book_isbn, book_name, book_price, book_img, book_pomo } = ctx.request.body
        
        if((book_isbn), (book_name), (book_price), (book_img), (book_pomo)) {
            await db.execute(`
                update books set 
                    book_name = ?, book_price = ?, book_img = ?, book_pomo = ?
                where book_isbn = ?
            `,[book_name, book_price, book_img, book_pomo, book_isbn])
            
            ctx.body = {'status':'success'}
            return
        }
    })
    .post('/delete/book', async (ctx) => {
        let { id } = ctx.request.body

        if(id) {
            await db.execute(`DELETE FROM books where book_isbn = ?`, [id])
            ctx.body = {'status':'success'}
            return
        }
    })

    app.use(cors())
    app.use(bodyparser())
    app.use(router.routes())

    app.listen('3000', () => {
        console.log('start server on port 3000')
    })
}

main()



