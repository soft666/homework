import { domUtil } from './lib/domUtil'

window.editBook = function (id) {
    let bookName = document.getElementById('card-header-' + id).textContent
    let bookImg = document.getElementById('img-' + id).currentSrc
    let bookPrice = document.getElementById('price-' + id).textContent

    $('#ebookId').val(id)
    $('#ebookTitle').val(bookName)
    $('#ebookSPrice').val(bookPrice)
    $('#ebookImg').val(bookImg)

    console.log(bookImg)
}

window.editBookSave = function () {
    let id = $('#ebookId').val()
    let bid = $('#bookId').val()
    let bname = $('#ebookTitle').val()
    let bimg = $('#ebookImg').val()
    let bprice = $('#ebookSPrice').val()

    $('#card-header-' + id).html(bname)
    $('#img-' + id).attr('src', bimg)
    $('#price-' + id).html(bprice)

    $('#modelEdit').modal('hide')
}

window.addBook = function () {
    let bookID = $('#bookId').val()
    let bookTitle = $('#bookTitle').val()
    let bookSPrice = $('#bookSPrice').val()
    let bookDate = $('#bookDate').val()
    let bookImg = $('#bookImg').val()

    let html = `
        <div class="col-md-3" style="margin-top: 5px;" id="row-${bookID}">
            <div class="card">
                <div class="card-header bg-transparent" id="card-header-${bookID}">${bookTitle}</div>
                <div class="card-body" style="text-align: center;">
                    <img class="card-img-top" id="img-${bookID}" src="${bookImg}" style="width: 100px;">
                    <h6 class="card-title" style="margin-top: 10px">Price : <span id="price-${bookID}">$${bookSPrice}</span>
                        <button class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modelEdit" id="edit-4" onclick="editBook(${bookID})">
                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                        </button>
                        <button class="btn btn-primary btn-sm" id="edit-4" onclick="delBook(${bookID})">
                        <i class="fa fa-trash-o" aria-hidden="true"></i>
                        </button>
                    </h6>
                </div>
            </div>
        </div>
    `

    $('#exampleModal').modal('hide')
    $('#booklist').append(html)
}

document.getElementById("openmodeladd").addEventListener("click", function () {
    $('#bookId').val("")
    $('#bookTitle').val("")
    $('#bookSPrice').val("")
    $('#bookDate').val("")
    $('#bookImg').val("")
})

window.delBook = function (id) {
    let r = confirm("You delet book ?");
    if (r == true) {
        $('#row-' + id).remove()
    } else {
    }


}

window.ApiAddBook() = function () {

    let bookID = $('#bookId').val()
    let bookTitle = $('#bookTitle').val()
    let bookSPrice = $('#bookSPrice').val()
    let bookDate = $('#bookDate').val()
    let bookImg = $('#bookImg').val()

    let url = 'http://localhost:3000/add/book';
    let data = { 
                    book_isbn: bookID,
                    book_name: bookTitle,
                    book_price: bookSPrice,
                    book_img: bookImg,
                    book_pomo: bookDate,
               }

    fetch(url, {
        method: 'PUT', // or 'PUT'
        body: JSON.stringify(data),
        headers: new Headers({
            'Content-Type': 'application/json'
        })
    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response))

}

// document.getElementById("addBook").addEventListener("click", function(){
//     console.log('addbook')  
//     let bookID = $('#bookId').val()
//     let bookTitle = $('#bookTitle').val()
//     let bookSPrice = $('#bookSPrice').val()
//     let bookDate = $('#bookDate').val()
//     let bookImg = $('#bookImg').val()

//     let newItemID = 'card-header-'+bookID
//     let newCardID = 'newCardID'+bookID
//     let newHeaderID = 'newHeaderID'+bookID
//     let newBodyID = 'newBodyID'+bookID
//     let newImgID = 'newImgID'+bookID
//     let newTitleID = 'newTitleID'+bookID
//     let newAddCarID = 'newAddCarID'+bookID

//     let newItem = document.createElement('div')
//         newItem.className = ('col-md-3')
//         newItem.style = ('margin-top: 5px')
//         newItem.id = (newItemID)
//     document.getElementById("booklist").appendChild(newItem)

//     let newCard = document.createElement('div')
//         newCard.className = ('card')
//         newCard.id = (newCardID)
//     document.getElementById(newItemID).appendChild(newCard)

//     let newCard_header = document.createElement('div')
//         newCard_header.className = ('card-header bg-transparent')
//         newCard_header.id = (newHeaderID)
//     let node_Card_header = document.createTextNode(bookTitle)
//         newCard_header.appendChild(node_Card_header);
//     document.getElementById(newCardID).appendChild(newCard_header)

//     let newCard_body = document.createElement('div')
//         newCard_body.className = ('card-header bg-transparent')
//         newCard_body.id = (newBodyID)
//         newCard_body.style = ('text-align: center;')
//     document.getElementById(newCardID).appendChild(newCard_body)

//     let newCard_img_top = document.createElement('img')
//         newCard_img_top.className = ('card-img-top')
//         newCard_img_top.id = ('img-'+bookID)
//         newCard_img_top.style = ('width: 100px;')
//         newCard_img_top.setAttribute("src", bookImg)
//     document.getElementById(newBodyID).appendChild(newCard_img_top)

//     let newCard_title = document.createElement('h6')
//         newCard_title.id = (newTitleID)
//     let node_newCard_title = document.createTextNode("Price : $")
//         newCard_title.appendChild(node_newCard_title);
//     document.getElementById(newBodyID).appendChild(newCard_title)

//     let new_span = document.createElement('span')
//         new_span.id = ('price-'+bookID)
//     let node_span = document.createTextNode(bookSPrice)
//     document.getElementById(newTitleID).appendChild(new_span)

//     let newCard_addcar = document.createElement('button')
//         newCard_addcar.className = ('btn btn-primary btn-sm')
//     let onclick = document.createAttribute('onclick')
//         onclick.value = 'editBook('+bookID+')'
//         newCard_addcar.setAttributeNode(onclick)
//         newCard_addcar.id = ('edit'+bookID)
//     document.getElementById(newTitleID).appendChild(newCard_addcar)

//     let newIconAdd = document.createElement('i')
//     newIconAdd.className = ('fa fa-pencil-square-o')
//     document.getElementById('edit'+bookID).appendChild(newIconAdd)

//     let newCard_delcar = document.createElement('button')
//         newCard_delcar.className = ('btn btn-primary btn-sm')
//         newCard_delcar.style = ('margin-left: 5px;')
//         newCard_delcar.id = ('del'+bookID)
//     document.getElementById(newTitleID).appendChild(newCard_delcar)

//     let newIconDele = document.createElement('i')
//     newIconDele.className = ('fa fa-trash-o')
//     document.getElementById('del'+bookID).appendChild(newIconDele)

//     $('#exampleModal').modal('hide')
// })
