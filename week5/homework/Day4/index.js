const Koa = require('koa')
const server = require('koa-static')
const fromidable = require('koa2-formidable')

app.use(server('public'))
app.use(fromidable())

const PORT = 3000

app.listen(PORT)
console.log(`Service is running on port ${PORT}.`)