import React from 'react';
import { Card, Input, Button, Row, Col } from 'antd';
import './App.css';
import ButtonAntd from './components/ButtonAntd.js'

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <Card style={{ width: '400px' }}>
          <p><Input size="large" /></p>
          <Row>
            <Col span={6} style={{ padding: '2px' }}><ButtonAntd></ButtonAntd></Col>
            <Col span={6} style={{ padding: '2px' }}><Button type="primary" size="large" style={{ width: '100%' }}>+/-</Button></Col>
            <Col span={6} style={{ padding: '2px' }}><Button type="primary" size="large" style={{ width: '100%' }}>%</Button></Col>
            <Col span={6} style={{ padding: '2px' }}><Button type="primary" size="large" style={{ width: '100%' }}>÷</Button></Col>
          </Row>
          <Row>
            <Col span={6} style={{ padding: '2px' }}><Button type="primary" size="large" style={{ width: '100%' }}>1</Button></Col>
            <Col span={6} style={{ padding: '2px' }}><Button type="primary" size="large" style={{ width: '100%' }}>2</Button></Col>
            <Col span={6} style={{ padding: '2px' }}><Button type="primary" size="large" style={{ width: '100%' }}>3</Button></Col>
            <Col span={6} style={{ padding: '2px' }}><Button type="primary" size="large" style={{ width: '100%' }}>-</Button></Col>
          </Row>
          <Row>
            <Col span={6} style={{ padding: '2px' }}><Button type="primary" size="large" style={{ width: '100%' }}>4</Button></Col>
            <Col span={6} style={{ padding: '2px' }}><Button type="primary" size="large" style={{ width: '100%' }}>5</Button></Col>
            <Col span={6} style={{ padding: '2px' }}><Button type="primary" size="large" style={{ width: '100%' }}>6</Button></Col>
            <Col span={6} style={{ padding: '2px' }}><Button type="primary" size="large" style={{ width: '100%' }}>x</Button></Col>
          </Row>
          <Row>
            <Col span={6} style={{ padding: '2px' }}><Button type="primary" size="large" style={{ width: '100%' }}>7</Button></Col>
            <Col span={6} style={{ padding: '2px' }}><Button type="primary" size="large" style={{ width: '100%' }}>8</Button></Col>
            <Col span={6} style={{ padding: '2px' }}><Button type="primary" size="large" style={{ width: '100%' }}>9</Button></Col>
            <Col span={6} style={{ padding: '2px' }}><Button type="primary" size="large" style={{ width: '100%' }}>+</Button></Col>
          </Row>
          <Row>
            <Col span={12} style={{ padding: '2px' }}><Button type="primary" size="large" style={{ width: '100%' }}>0</Button></Col>
            <Col span={6} style={{ padding: '2px' }}><Button type="primary" size="large" style={{ width: '100%' }}>.</Button></Col>
            <Col span={6} style={{ padding: '2px' }}><Button type="primary" size="large" style={{ width: '100%' }}>=</Button></Col>
          </Row>
        </Card>
      </div>
    );
  }
}

export default App;
