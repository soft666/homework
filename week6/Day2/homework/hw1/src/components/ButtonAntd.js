import React from 'react';
import { Button } from 'antd';

class ButtonAntd extends React.Component {
  
  render() {
    return (
      <Button type="primary" size="large" style={{ width: '100%' }}>AC-</Button>
    )
  }
}

export default ButtonAntd;