import React, { Component } from 'react';
import { Form, Icon, Input, Button, Card } from 'antd';
import './App.css';

class App extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      username : '',
      password : '',
      total: ''
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleChangeP = this.handleChangeP.bind(this);
    this.handlesubmit = this.handlesubmit.bind(this);
  }

  handleChange(event) {
    this.setState({username: event.target.value});
    console.log(event.target.value)
  }

  handleChangeP(event) {
    this.setState({password: event.target.value});
    console.log(event.target.value)
  }

  handlesubmit(event) {
    alert('A name was submitted: ' + this.state.username);
    console.log('asdasd')
    event.preventDefault();
  }

  render() {

    const FormItem = Form.Item;
    return (
        <div className="App">
        
            <h1>Welcome to my application</h1>
            <Card title="Sing In" style={{ width: 300, margin: 'auto' }}>
              <Form className="login-form" onSubmit={this.handleSubmit}>
                <FormItem>
                    <Input prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />} value={this.state.username} onChange={this.handleChange}/>
                </FormItem>
                <FormItem>
                    <Input prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />} type="password" value={this.state.password} onChange={this.handleChangeP}/>
                </FormItem>
                <FormItem>
                    <Button type="primary" htmlType="submit" className="login-form-button">
                      Sign in
                    </Button>
                    <a className="login-form-forgot" href="">Forgot password</a>
                </FormItem>
                <hr></hr>
                <FormItem>
                    <Button type="primary" htmlType="submit" className="login-form-button" onClick={(e) => this.handlesubmit(e)}>
                      Sign up
                    </Button>
                </FormItem>
              </Form>
            </Card>
        </div>
    );
  }
}

export default App;
