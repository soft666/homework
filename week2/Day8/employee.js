class Employee {
    constructor(firstname, lastname, salary) {
        this.firstname = firstname
        this.lastname = lastname
        this.salary = salary; // simulate private variable
    }
    setSalary(newSalary) { // simulate public method
        if(newSalary > this.salary) { 
        // ถ้ามีเงินเดือนใหม่มีค่ามากกว่า 
            // this._salary
            return newSalary 
        } else {
        // ถ้าเงินเดือนใหม่มีค่าน้อยกว่าเท่ากับ 
            this.salary
            return false 
        }
    }
    getSalary () {  // simulate public method
        return this.salary;
    };
    work(employee) {
        // leave blank for child class to be overidden
    }
    leaveForVacation(year, month, day) {

    }
}

exports.Employee = Employee