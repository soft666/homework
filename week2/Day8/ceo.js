let fs = require('fs')
let {Employee} = require('./employee.js')
let {Programmer} = require('./programmer.js')

class CEO extends Employee {
    constructor(firstname, lastname, salary) {
        super(firstname, lastname, salary)
        this.dressCode = 'suit'
        this.employeeRaw = this.readFileJS()
    }
    getSalary(){  // simulate public method
        return super.getSalary()*2;
    };
    work (employee) {  // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    }
    increaseSalary(employee, newSalary) {
        if(!employee.setSalary(newSalary)) {
            console.log(employee.firstname+" salary is less than before!!")
        } else {
            console.log(employee.firstname+" salary has been set to"+ newSalary)
        }
    }
    _golf () { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);
    }

    gossip (name,txt) {
        console.log("Hey "+name.firstname+" "+txt)
    }

    _fire (employee) {
        this.dressCode = 'tshirt';
        console.log(employee.firstname+" has been fired! Dress with :" + this.dressCode);
    }

    _hire (employee) {
        this.dressCode = 'tshirt';
        console.log(employee.firstname+" has been hired! Dress with :" + this.dressCode);
    }

    _seminar () {
        this.dressCode = 'suit';
        console.log("He is going to seminar Dress with :" + this.dressCode);
    }

    readFileJS() {
        let self = this
        this.employeeRaw = new Promise((resolve, reject) => {
            fs.readFile('./homework1.json', "utf8", (err, data) => {
                if (err) {
                    reject(err)
                } else {
                    let dataJSON = JSON.parse(data).map((data) => {
                        return new Programmer(data.firstname,data.lastname,data.salary,data.id)
                    })
                    self.employees = dataJSON
                    resolve(JSON.parse(data))
                }
            })
        })
    }

    // async createArr() {
    //     return this.employees = this.employees
    // }

    // // async createArr() {
    // //     let employeeRawNew = await this.employeeRaw
    // //     let self = this
    // //     this.employees = new Promise((resolve, reject) => {
    // //         let employees = []
    // //         let dataJSON = employeeRawNew.map((data) => {
    // //             return new Programmer(data.firstname,data.lastname,data.salary,data.id)
    // //         })
    // //         if(dataJSON) {
    // //             self.employees = dataJSON
    // //             console.log(self.employees)
    // //             resolve(dataJSON)
    // //         }
    // //         else {
    // //             reject()
    // //         }
                
    // //     })
    // // }



}


exports.CEO = CEO