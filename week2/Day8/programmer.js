let {Employee} = require('./employee.js')

class Programmer extends Employee {
    constructor(firstname, lastname, salary, id) {
        super(firstname, lastname, salary)
            this.id = id
            this.type = "BackEnd"
    }

    CreateWebsite() {

    }

    FixPC() {

    }

    installWindows() {

    }
}

exports.Programmer = Programmer