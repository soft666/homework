let fs = require("fs")

let readJson = () => {
    return new Promise((resolve, reject) => {
        fs.readFile('homework1.json', 'utf8', (err, data) => {
            if(err) {
                reject(err)
            } else {
                resolve(JSON.parse(data))
            }
        })
    })
}

let employee = async () => {
    try {
        let data = await readJson()
        return data
    } catch (err) {
        return err
    }
    
}

function addYearSalary(row) {
    row.yearSalary = row.salary*12
    return row
}

function addNextSalary(row) {
    row.NextSalary = [...Array(3)].map((_, i) => Math.round(row.salary * (1.1 ** i)))

    // row.NextSalarys = [...Array(3)].map((_,i) => {
    //     if(i == 0) {
    //         row.salary = row.salary 
    //     } else {
    //         row.salary = row.salary* (1.1 * (i + 1))
    //     }
    //    return row.salary
    // })
    return row
}

function addAdditionalFields(employees) {
    employees.map(addYearSalary)
    employees.map(addNextSalary)
    return employees
}

async function main() {
    let employees = await employee()
    
    console.log(addAdditionalFields(employees))

}

main()