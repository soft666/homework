let input1 = [1,2,3,4,5]
let input2 = {a:1,b:2}
let input3 = [1,2,{a:1,b:2}] 
let input4 = [1,2,{a:1,b:{c:3,d:4}}]

function cloneNewObj(data) {

    if(Array.isArray(data)) {
        newArr = []
        for (const key_1 in data) {
            console.log(data[key_1])
            if(typeof(data[key_1]) !== 'object') {
                newArr[key_1] = data[key_1]
            }
            if(typeof(data[key_1]) === 'object') {
                newObj = {}
                for (const key_2 in data[key_1]) {
                    newObj2 = {}
                    if(typeof(data[key_1][key_2]) !== 'object') {
                        newObj[key_2] = data[key_1][key_2]
                    } else {
                        for (const key_3 in data[key_1][key_2]) {
                            newObj2[key_3] = data[key_1][key_2][key_3]
                        }
                        newObj[key_2] = newObj2
                    }
                }
                newArr.push(newObj)
            }
        }

    } else {
        newArr = {}
        for (const key_1 in data) {
            newArr[key_1] = data[key_1]
        }
    }
    return newArr
}

let output1 = cloneNewObj(input1)
let output2 = cloneNewObj(input2)
let output3 = cloneNewObj(input3)
let output4 = cloneNewObj(input4)
console.log("----------------------------------------")
input1[1] = 4
console.log('input1 : ',input1)
console.log('newArr : ',output1)
console.log("----------------------------------------")
input2.b = 4
console.log('input2 : ',input2)
console.log('newArr : ',output2)
console.log("----------------------------------------")
input3[2].b = 10
console.log('input3 : ',input3)
console.log('newArr : ',output3)
console.log("----------------------------------------")
input4[2].b.c = 9
console.log('input4 : ',input4)
console.log('newArr : ',output4)
console.log("----------------------------------------")
// console.log(arr)