let fs = require("fs")

let readJson = () => {
    return new Promise((resolve, reject) => {
        fs.readFile('homework1.json', 'utf8', (err, data) => {
            if(err) {
                reject(err)
            } else {
                resolve(JSON.parse(data))
            }
        })
    })
}

let employee = async () => {
    try {
        let data = await readJson()
        return data
    } catch (err) {
        return err
    }
    
}

function addYearSalary(row) {
    row.yearSalary = row.salary*12
    return row
}

function addNextSalary(row) {
    row.NextSalary = [...Array(3)].map((_, i) => Math.round(row.salary * (1.1 * (i + 1))))
    return row
}

function addAdditionalFields(employees) {
    employees.map(addYearSalary)
    employees.map(addNextSalary)
    return employees
}

function newObject(obj) {
    arr = []
    for (const key1 in obj) {
        newObj = {}
        for (const key in obj[key1]) {
            newObj[key] = obj[key1][key]
        }
        arr.push(newObj)
    }
    return arr
}

async function main() {
    let employees = await employee()
    let newEmployees = newObject(employees)
    console.log(newEmployees[0].salary = 0 )
    let t1 = addYearSalary(employees[0].salary)
    console.log(employees[0].salary)
    

}

main()