let fs = require('fs')

function saveUserDatabase(user, filename, callback) {
  let json_string = JSON.stringify(user);
  if (json_string.length > 0) {
    fs.writeFile(filename, json_string, "utf8", function(err) {
      if (err) 
        callback(err);
      else 
        callback(null);
    });
  }
}

exports.saveUserDatabase = saveUserDatabase