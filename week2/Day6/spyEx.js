let fs = require("fs")
let assert = require("assert");
let sinon = require("sinon")
let saveUse = require("./saveDatabase.js")

describe("lab 1 test double", () => {
  describe("#SpyExmple()", () => {
    it("should write JSON string into output6_1.txt. check with spy", function() {
      const save = sinon.spy(fs, "writeFile");
      const user = {
        firstname: "Somchai",
        lastname: "Sudlor"
      };
      
      let dummyCallbackFunction = function(err) {};
      saveUse.saveUserDatabase(user, "output6_1.txt", dummyCallbackFunction);
      saveUse.saveUserDatabase(user, "output6_1.txt", dummyCallbackFunction);

      save.restore();

      sinon.assert.calledTwice(save);
      sinon.assert.callCount(save, 2);
      sinon.assert.calledWith(save, "output6_1.txt");
    });
  });
});
