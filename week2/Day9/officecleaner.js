let {Employee} = require('./employee.js')

class OfficeCleaner extends Employee {
    constructor(firstname, lastname, salary, id, dressCode) {
        super(firstname, lastname, salary)
            this.id = id
            this.dressCode = dressCode
        }

    work() {
        this.Clean()
        this.KillCoachroach()
        this.DecorateRoom()
        this.WelcomeGuest()
        // console.log("OfficeCleaner-------------")
    }

    Clean() {
        console.log('Clean')
    }

    KillCoachroach() {
        console.log('KillCoachroach')
    }

    DecorateRoom() {
        console.log('DecorateRoom')
    }

    WelcomeGuest() {
        console.log('WelcomeGuest')
    }
}

exports.OfficeCleaner = OfficeCleaner