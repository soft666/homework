let { Employee } = require("./employee.js");
let { OfficeCleaner } = require("./officecleaner.js");
let { Programmer } = require("./programmer");
let fs = require("fs");

class CEO extends Employee {
  constructor(firstname, lastname, salary, id, dressCode) {
    super(firstname, lastname, salary)
      this.id = id
      this.dressCode = dressCode
    }
  getSalary() {
    // simulate public method
    return super.getSalary() * 2;
  }

  work(employee) {
    // simulate public method
    this._fire(employee);
    this._hire(employee);
    this._seminar()
    this._golf();
  }

  increaseSalary(employee, newSalary) {
    if (!employee.setSalary(newSalary)) {
      console.log(employee.firstname + " salary is less than before!!");
    } else {
      console.log(employee.firstname + " salary has been set to" + newSalary);
    }
  }
  _golf() {
    // simulate private method
    this.dressCode = "golf_dress";
    console.log(
      "He goes to golf club to find a new connection." +
        " Dress with :" +
        this.dressCode
    );
  }

  gossip(name, txt) {
    console.log("Hey " + name.firstname + " " + txt);
  }

  _fire(employee) {
    this.dressCode = "tshirt";
    console.log(
      employee.firstname + " has been fired! Dress with :" + this.dressCode
    );
  }

  _hire(employee) {
    this.dressCode = "tshirt";
    console.log(
      employee.firstname + " has been hired! Dress with :" + this.dressCode
    );
  }

  _seminar() {
    this.dressCode = "suit";
    console.log("He is going to seminar Dress with :" + this.dressCode);
  }

  talk(message) {
    console.log(message)
  }

  reportRobot(self, robotMessage) {
    self.talk(robotMessage);
  }

  readFileJS() {
    let self = this
      this.employeeRaw = new Promise((resolve, reject) => {
          fs.readFile('./employee9.json', "utf8", (err, data) => {
              if (err) {
                  reject(err)
              } else {
                  let DataParse = JSON.parse(data)

                  let employee = DataParse.map((data) => {
                    if(data.role == 'CEO')
                      return new CEO(data.firstname, data.lastname, data.salary, data.role, data.dressCode)
                    if(data.role == 'Programmer')
                      return new Programmer(data.firstname, data.lastname, data.salary, data.role, data.type)
                    if(data.role == 'OfficeCleaner')
                      return new OfficeCleaner(data.firstname, data.lastname, data.salary, data.role, data.dressCode)
                  })
                  self.employees = employee
                  resolve(self.employees)
              }
          })
      })
      return this.employeeRaw
  }

}

// Run()
exports.CEO = CEO;
