let lodash = require('lodash')

class MyUtility {
    
    _assign(object, sorces) {
        return lodash.assign(object,sorces)
    }

    _times(n,type) {
        return lodash.times(n,type)
    }

    _keyBy(collection, identity) {
        return lodash.keyBy(collection, identity)
    }

    _cloneDeep(value) {
        return lodash.cloneDeep(value)
    }

    _filter(collection, identity) {
        return lodash.filter(collection, identity)
    }

    _sortBy(collection, identity) {
        return lodash.sortBy(collection, identity)
    }

}

let MyUtilitys = new MyUtility


console.log(MyUtilitys._times(3, String))