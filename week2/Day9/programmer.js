let {Employee} = require('./employee.js')

class Programmer extends Employee {
    constructor(firstname, lastname, salary, id, dressCode) {
        super(firstname, lastname, salary)
            this.id = id
            this.dressCode = dressCode
        }

    work() {
        this.CreateWebsite()
        this.FixPC()
        this.installWindows()
        // console.log('Programmer-------------')
    }

    CreateWebsite() {
        console.log('CreateWebsite')
    }

    FixPC() {
        console.log('FixPC')
    }

    installWindows() {
        console.log('installWindows')
    }  
}

exports.Programmer = Programmer