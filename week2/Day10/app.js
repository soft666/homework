const Koa = require("koa");
const render = require("koa-ejs");
const path = require("path");
const {Database} = require('./lib/db.js')
const app = new Koa();
const otherMiddleware = require('./controller/routes.js')(app);


render(app, {
  root: path.join(__dirname, "views"),
  layout: "template",
  viewExt: "ejs",
  cache: false,
  debug: true
});


// app.use(async (ctx, next) => {
//   try {

//     // const connection = await mysql.createConnection({
//     //   host: "localhost",
//     //   port: "8889",
//     //   user: "root",
//     //   password: "root",
//     //   database: "codecamp"
//     // });
//     // connection.connect()

//     const [rows, fields] = await Database.query('SELECT * FROM user');
//     await ctx.render("user", { user : rows });
//     await next();
//   } catch (err) {
//     ctx.status = 400;
//     ctx.body = `Uh-oh: ${err.message}`;
//   }
// });

app.listen(3000);
