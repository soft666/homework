const { queryDB } = require('../models/homework10_1.js')

module.exports = ((app) => {
  app.use(async (ctx, next) => {
    try {
      
      await ctx.render("user", { user : await queryDB.getUser() });
      await next();
    } catch (err) {
      ctx.status = 400;
      ctx.body = `Uh-oh: ${err.message}`;
    }
  })
})

