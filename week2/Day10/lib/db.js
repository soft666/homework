const mysql = require('mysql2/promise');
const { Config } = require('../config/database.js')

class Database {

    constructor() {
        this._connection()
    }

    async _connection() {
        try {
            this.connection = await mysql.createPool({
              connectionLimit: 10,
              host: Config.host,
              port: Config.port,
              user: Config.user,
              password: Config.password,
              database: Config.database
            })
          } catch (err) {
           console.log(err)
        }
    }

    async query(sql) {
        try {
            let result = await this.connection.execute(sql)
            
            return result
        } catch (err) {
            console.log(err)
        }
    }

    async query_pool(sql) {
        try {
            let getConnect = await this.connection.getConnection(() => {})
            let result = await getConnect.execute(sql)
            getConnect.release()
            return result
        } catch (err) {
            console.log(err)
        }
    }

    

}

module.exports.Database = new Database()

// let RunAsync = async () => {
//     try {
//         const Databases = new Database()
//         const [rows, fields] = await Databases.query_pool('SELECT * FROM user');
//         console.log(rows)
//     } catch (error) {
//         console.log(error)
//     }
    
// }
// RunAsync()

  // const connection = await mysql.createConnection({
    //   host: "localhost",
    //   port: "8889",
    //   user: "root",
    //   password: "root",
    //   database: "codecamp"
    // });
    // connection.connect()
