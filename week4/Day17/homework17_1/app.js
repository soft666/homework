const Koa = require('koa')
const session = require('koa-session')
const db = require('./lib/db.js')
const config = require('./config/db.json')

const sessionConfig = {
    key: 'sess',
    maxAge: 10 * 1000,
    httpOnly: true,
    store: {
        async get(key, maxAge, { rolling }) {
            try {
                const DB = await db(config)
                let [rows] = await DB.execute("SELECT sess FROM `session` WHERE `key` = ?", [key])
                let debase64 = JSON.parse(new Buffer(rows[0].sess, 'base64').toString('ascii'))
                return debase64
            } catch (error) {
                console.log(error)
            }
        },
        async set(key, sess, maxAge, { rolling }) {
            console.log('set:', key)
            let sess_base64 = new Buffer(JSON.stringify(sess)).toString('base64')
            const DB = await db(config)
            await DB.execute("INSERT INTO `session` (`key`, `sess`) VALUES (?, ?)",[key, sess_base64])
        },
        async destroy(key) {
            console.log('delect')
            // await DB.execute("delect from `session` WHERE `key` = ?", [key])
        }
    }
}

const app = new Koa()
app.keys = ['supersecret']
app
    .use(session(sessionConfig, app))
    .use(handler)
    .listen(3000)

function handler(ctx) {
    let log = ctx.session.views = 'MaximuM'
    ctx.body = `${log}`
}
