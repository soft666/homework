module.exports = function (db) {
    return {
        async todoList() {
            const [rows] = await db.execute(`select * from todo_msg`)
            return rows
        },
        async todoListPost(data) {
            const rows = await db.execute(`insert into todo_msg (msg) values(?)`,[data.msg])
            return rows
        },
        async todosId(id) {
            const [rows] = await db.execute(`select * from todo_msg where id = ?`,[id])
            return rows
        },
        async todoUpdate(data) {
            const rows = await db.execute(`update todo_msg set msg = ? where id = ?`,[data.msg, data.id])
            return rows
        },
        async todoRemove(id) {
            const rows = await db.execute(`delete from todo_msg where id = ?`,[id])
            return rows
        },
        async todoComplete(id) {
            const rows = await db.execute(`update todo_msg set status = 'true' where id = ?`,[id])
            return rows
        },
        async todoinComplete(id) {
            const rows = await db.execute(`update todo_msg set status = 'false' where id = ?`,[id])
            return rows
        }
    }
}
