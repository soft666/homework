let router = require('koa-router')()

module.exports = ((app, todo) => {
    router.get('/todos', async (ctx, next) => {
        //Todo.list
        ctx.body = await todo.todoList()
    })
    .post('/todos', async (ctx, next) => {
        // await todo.todoListPist()
        let { msg } = ctx.request.body
        if(msg) {
             const addTodo = await todo.todoListPost(ctx.request.body)
            ctx.body = addTodo
        } else {
            ctx.body = "value not found"
        }
    })
    .get('/todos/:id', async (ctx) => {
        // Todo.get
        let id = ctx.params.id
        console.log(id)
        if(id) {
            const getTodo = await todo.todosId(id)
            ctx.body = getTodo
        } 
    })
    .patch('/todos/:id', async (ctx) => {
        // Todo.update
        let { msg } = ctx.request.body
        if(msg !== undefined) {
            ctx.request.body.id = ctx.params.id
            const addTodo = await todo.todoUpdate(ctx.request.body)
            ctx.body = addTodo
        } else {
            ctx.body = "value not found"
        }
    })
    .delete('/todos/:id', async (ctx) => {
        // Todo.remove
        let id = ctx.params.id
        if(id) {
            ctx.body = await todo.todoRemove(id)
        } 
    })
    .put('/todos/:id/complete', async (ctx) => {
        // Todo.complete
        let id = ctx.params.id
        if(id) {
            ctx.body = await todo.todoComplete(id)
        } 
    })
    .delete('/todos/:id/complete', async (ctx) => {
        // Todo.incomplete
        let id = ctx.params.id
        if(id) {
            ctx.body = await todo.todoinComplete(id)
        } 
    })

    app.use(router.routes())
})