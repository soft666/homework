const Koa = require('koa')
const bodyParser = require('koa-bodyparser')
const Databases = require('./lib/db.js')
const config = require('./config/db.json')
const route = require('./router/routre.js')
const todo = require('./model/todo.js')

let main = async () => {
    const app = new Koa()
    app.use(bodyParser())

    const db = await Databases(config)

    route(app, todo(db))

    app.listen(3000, () => {
        console.log('Start server on port 3000')
    })
}

main()