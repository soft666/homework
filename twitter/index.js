const Koa = require('koa')
const Database = require('./src/lib/database.js')
const ConfigDB = require('./src/config/database.json')
// const router = require('./src/router/router.js')
const router = require('koa-router')()
const bodyparser = require('koa-bodyparser')
const session = require('koa-session') 
const sharp = require('sharp')
const multer = require('koa-multer')
const fs = require('fs')
//repo
const repoUser = require('./src/repo/user.js')
const repoTweet = require('./src/repo/tweet.js')
const repoNoti = require('./src/repo/notifications.js')
//controller
const makeCltAuth = require('./src/controller/auth.js')
const makeCltUser = require('./src/controller/user.js')
const makeCltTweet = require('./src/controller/tweet.js')
const makeCltUpload = require('./src/controller/upload.js')

const upload = multer({ dest: 'upload/' }) 

const main = async () => {
    const db = await Database(ConfigDB)
    const sessionStore = {}
    const sessionConfig = { 
        key: 'sess',
        maxAge: 3600 * 1000,
        httpOnly: true,
        store: {
            async get (key, maxAge, { rolling }) {
                try {
                    let [rows] = await db.execute(`select value_session from user_session where key_session = ?`, [key])
                    let debase64 = JSON.parse(new Buffer(rows[0].value_session, 'base64').toString('ascii'))
                    return debase64
                } catch (error) {
                    console.error(error)
                }
            },
            async set (key, sess, maxAge, { rolling }) {
                let sess_base64 = new Buffer(JSON.stringify(sess)).toString('base64')
                await db.execute(`insert into user_session (key_session, value_session) values(?, ?)`, [key, sess_base64])
            },
            async destroy (key) {
                delete sessionStore[key]
            }
        } 
    } 
    

    const cltAuth = makeCltAuth(db, repoUser)
    const cltUser = makeCltUser(db, repoUser, repoNoti)
    const cltTweet = makeCltTweet(db, repoTweet)

    const cltUpload = makeCltUpload(upload, sharp, fs)

    router.get('/', (ctx) => {
        ctx.body = 'Koa'
    })
    //Auth
    router.post('/auth/signup', cltAuth.signup)
    .post('/auth/signin', cltAuth.signin)
    .get('/auth/signout', cltAuth.signout)
    .post('/auth/verify', cltAuth.verify)
    //user
    router.patch('/user/:id', cltUser.update)
    .put('/user/:id/follow', cltUser.follow)
    .delete('/user/:id/follow', cltUser.unfollow)
    .get('/user/:id/follow', cltUser.followList)
    .get('/user/:id/followed', cltUser.followedList)
    // //upload
    router.post('/upload', cltUpload.photo)

    // //Tweet
    // router.get('/tweet', (ctx) => {
    //     ctx.body = '/tweet'
    // })
    // .post('/tweet', (ctx) => {
    //     ctx.body = '/tweet'
    // })
    // .put('/tweet/:id/like', (ctx) => {
    //     ctx.body = '/tweet/:id/like'
    // })
    // .delete('/tweet/:id/like', (ctx) => {
    //     ctx.body = '/tweet/:id/like'
    // })
    // .put('/tweet/:id/retweet', (ctx) => {
    //     ctx.body = '/tweet/:id/retweet'
    // })
    // .post('/tweet/:id/reply', (ctx) => {
    //     ctx.body = '/tweet/:id/reply'
    // })

    // // //Notification
    // router.get('/notification', (ctx) => {
    //     ctx.body = '/notification'
    // })


    // Direct Message
    router.get('/message', cltUser.chatList)
    .get('/message/:userId', cltUser.chatUser)
    .post('/message/:userId', cltUser.chatSender)

    
    
    const app = new Koa()


    app.keys = ['supersecret'] 
    app.use(session(sessionConfig, app)) 
    app.use(requestLogger)
    app.use(bodyparser())
    app.use(router.routes())
    app.listen(3000, () => {
        console.log("start server on port 3000")
    })
}

main()

async function requestLogger (ctx, next) {
    const start = process.hrtime()
    await next()
    const diff = process.hrtime(start)
    console.log(`${ctx.method} ${ctx.path} ${diff[0] * 1e9 + diff[1]}ns`)
}