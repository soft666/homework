module.exports = (upload, sharp, fs) => {
    return {
        async photo(ctx) {
            await upload.single('file')(ctx)
            const tempFile = ctx.req.file.path
            const outFile = tempFile+'.jpg'
            await sharp(tempFile)
            .resize(100, 100)
            .toFile(outFile)
            fs.unlink(tempFile, () => {})
            ctx.body = outFile 
        }
    }
}

