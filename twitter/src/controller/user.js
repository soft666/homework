module.exports = (db, repoUser, repoNoti) => {
    return {
        async update(ctx) {
            if(ctx.session.login) {
                let getUser = await repoUser.getUser(db, ctx.session.login)
                
                if(getUser.id != ctx.params.id) {
                    ctx.status = 403
                    ctx.body = {"error": "forbidden"}
                    return
                }
                    await repoUser.updateUser(db, ctx.params.id, ctx.request.body)
                    ctx.body = {}
                    return
            } 
                ctx.status = 401
                ctx.body = {"error": "unauthorized"}
        },
        async follow(ctx) {
            let id = ctx.params.id
            if(id) {
                if(!ctx.session.login) {
                    ctx.status = 401
                    ctx.body = {"error": "unauthorized"}
                    return
                }
                let getUser = await repoUser.getUser(db, ctx.session.login)
                await repoUser.follow(db, getUser.id, id)
                let noti = {"title": "follow","content": "follow", "photo":""}
                await repoNoti.create(db, id, noti)
                ctx.body = {}
            }
        },
        async unfollow(ctx) {
            let id = ctx.params.id

            if(id) {
                if(!ctx.session.login) {
                    ctx.status = 401
                    ctx.body = {"error": "unauthorized"}
                    return
                }
                
                let getUser = await repoUser.getUser(db, ctx.session.login)
                await repoUser.unfollow(db, getUser.id, id)
                ctx.body = {}
            }
        },
        async followList(ctx) {
            let id = ctx.params.id
            if(id) {
                ctx.body = await repoUser.followList(db, id)
                return
            }
        },
        async followedList(ctx) {
            let id = ctx.params.id
            if(id) {
                ctx.body = await repoUser.followedList(db, id)
                return
            }
        },
        async chatSender(ctx) {
            let userId = ctx.params.userId
            if(userId) {
                if(!ctx.session.login) {
                    ctx.status = 401
                    ctx.body = {"error": "unauthorized"}
                    return
                }

                let { content, type } = ctx.request.body
                if((content) && (type)) {
                    let getUser = await repoUser.getUser(db, ctx.session.login)
                    await repoUser.chatSender(db, getUser.id, userId, content, type)
                    let noti = {"title": "Chat","content": getUser.username, "photo":""}
                    await repoNoti.create(db, userId, noti)
                    ctx.body = {}
                    return
                }
            }
        },
        async chatUser(ctx) {
            let userId = ctx.params.userId 
            if(userId) {
                const chat = await repoUser.chatList(db, userId)
                ctx.body = chat
                return
            }
        },
        async chatList(ctx) {
            if(!ctx.session.login) {
                ctx.status = 401
                ctx.body = {"error": "unauthorized"}
                return
            }
                let getUser = await repoUser.getUser(db, ctx.session.login)
                const chat = await repoUser.chatList(db, getUser.id)
                ctx.body = chat
                return
        }
    }
}