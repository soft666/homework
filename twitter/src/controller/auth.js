module.exports = (db, repoUser) => {
    return {
        async signup(ctx) {

            const {username, email, password, name} = ctx.request.body

            if((username) && (email) && (password) && (name)) {
                try {
                    const result = await repoUser.create(db, ctx.request.body)
                    ctx.body = {"status": "success"}
                } catch (error) {
                    console.error(error)
                    ctx.body = {"status": "error"}
                }
                return
            }

        },
        async signin(ctx) {
            const {email, password} = ctx.request.body

            if((email) && (password)) {
                try {
                    const result = await repoUser.checkUser(db, ctx.request.body)
                    if(result.length == 0) {
                        ctx.status = 401
                        ctx.body = {"error": "wrong password"}
                        return
                    }
                       
                    let log = ctx.session.login = result[0].email
                    ctx.body = {}
                    
                } catch (error) {
                    console.error(error)
                }
                return
            }
        },
        async signout(ctx) {
            ctx.session = null
            ctx.body = {}
        },
        async verify(ctx) {
            const { token } = ctx.request.body

            if(token) {
                ctx.body = {}
            } else {
                ctx.status = 400
                ctx.body = {"error": "invalid token"}
            }

        }
    }
}