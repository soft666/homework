const router = require('koa-router')()

module.exports = (app) => {
    router.get('/', (ctx) => {
        ctx.body = 'Koa'
    })

    //Auth
    router.post('/auth/signup', (ctx) => {
        ctx.body = 'auth/signup'
    })
    .post('/auth/signin', (ctx) => {
        ctx.body = 'auth/signin'
    })
    .get('/auth/signout', (ctx) => {
        ctx.body = 'auth/signout'
    })
    .post('/auth/verify', (ctx) => {
        ctx.body = 'auth/verify'
    })
    
    //upload
    router.post('/upload', (ctx) => {
        ctx.body = '/upload'
    })

    //Tweet
    router.get('/tweet', (ctx) => {
        ctx.body = '/tweet'
    })
    .post('/tweet', (ctx) => {
        ctx.body = '/tweet'
    })
    .put('/tweet/:id/like', (ctx) => {
        ctx.body = '/tweet/:id/like'
    })
    .delete('/tweet/:id/like', (ctx) => {
        ctx.body = '/tweet/:id/like'
    })
    .put('/tweet/:id/retweet', (ctx) => {
        ctx.body = '/tweet/:id/retweet'
    })
    .post('/tweet/:id/reply', (ctx) => {
        ctx.body = '/tweet/:id/reply'
    })

    //Notification
    router.get('/notification', (ctx) => {
        ctx.body = '/notification'
    })


    // Direct Message
    router.get('/message', (ctx) => {
        ctx.body = '/message'
    })
    .get('/message/:userId', (ctx) => {
        ctx.body = '/message/:userId'

    })
    .post('/message/:userId', (ctx) => {
        ctx.body = '/message/:userId'
    })

    app.use(router.routes())
}