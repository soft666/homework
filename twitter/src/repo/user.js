module.exports = {
    create,
    checkUser,
    getUser,
    updateUser,
    changePhoto,
    changeCover,
    follow,
    unfollow,
    followList,
    followedList,
    chatSender,
    chatList
  }
  
  async function create (db, user) {
    const result = await db.execute(`
      insert into users (
        username, email, password, name, status
      ) values (
        ?, ?, ?, ?, ?
      )
    `, [
      user.username, user.email, user.password,
      user.name, 0
    ])
    return result[0].insertId
  }

  async function getUser (db, email) {
    const [result] = await db.execute(`
      select id, username, email, name, status 
      from users where email = ?
    `, [email])
    return result[0]
  }
  
  async function checkUser (db, user) {
    const [result] = await db.execute(`
      select email, password from users
      where email = ? and password = ?
    `, [user.email, user.password])
    return result
  }

  async function updateUser (db, id, user) {
    await db.execute(`
      update users set
        name = ?, photo = ?, cover = ?, location = ?, bio = ?, theme = ?, 
        birth_date_d = ?, birth_date_m = ?, birth_date_y = ?, show_y = ?, show_dm = ?
      where id = ?
    `, [user.name, user.profilePhoto, user.coverPhoto, user.location, user.bio, user.theme, user.birthDateD, user.birthDateM, user.birthDateY, user.showBirthDateY, user.showBirthDateDM, id])
  }

  async function changePhoto (db, userId, photoUrl) {
    await db.execute(`
      update users set
        photo = ?
      where id = ?
    `, [photoUrl, userId])
  }
  
  async function changeCover (db, userId, photoUrl) {
    await db.execute(`
      update users set
        cover = ?
      where id = ?
    `, [photoUrl, userId])
  }
  
  async function follow (db, followerId, followingId) {
    await db.execute(`
      insert into follows (
        follower_id, following_id
      ) values (
        ?, ?
      )
    `, [followerId, followingId])
  }
  
  async function unfollow (db, followerId, followingId) {
    await db.execute(`
      delete from follows
      where follower_id = ? and following_id = ?
    `, [followerId, followingId])
  }

  async function followList (db, followingId) {
    let [result] = await db.execute(`
      select id, username, photo, cover, bio from follows fw
      left join users us on us.id = fw.follower_id
      where fw.following_id = ?
    `, [followingId])
    return result
  }

  async function followedList (db, followerId) {
    let [result] = await db.execute(`
      select id, username, photo, cover, bio from follows fw
      left join users us on us.id = fw.following_id
      where fw.follower_id = ?
    `, [followerId])
    return result
  }

  async function chatSender (db, senderId, receiverId, content, type) {
    const result = await db.execute(`
      insert into user_chats (
        sender_id, receiver_id, content, type
      ) values (
        ?, ?, ?, ?
      )
    `, [senderId, receiverId, content, type])
    return result[0].insertId
  }

  async function chatList (db, UserId) {
    const [result] = await db.execute(`
      select * from user_chats where sender_id = ? or receiver_id = ?
    `, [UserId, UserId])
    return result
  }

