const mysql = require('mysql2/promise')

module.exports = async (config) => {
    const conn = await mysql.createPool(config)
    return {
        execute(sql, params = []) {
            return conn.execute(sql, params)
        }
    }
}

