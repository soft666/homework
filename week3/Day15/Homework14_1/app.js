const Koa = require("koa")
const render = require("koa-ejs")
const path = require("path");
const app = new Koa();
const router = require('./controller/routes.js')(app)


render(app, {
  root: path.join(__dirname, "views"),
  layout: "template",
  viewExt: "ejs",
  cache: false,
  debug: false
})



app.listen(3000, () => {
  console.log(`Server started on port 3000`)
});
