const mysql = require('mysql2/promise');
const { Config } = require('../config/database.js')

class Database {

    constructor() {
        this._connection()
    }

    async _connection() {
        try {
            this.connection = await mysql.createPool({
              connectionLimit: 100,
              host: Config.host,
              port: Config.port,
              user: Config.user,
              password: Config.password,
              database: Config.database
            })
          } catch (err) {
           console.log(err)
        }
    }

    async exc_pool(sql,id) {
        try {
            let db = await this.connection.getConnection()
            await db.beginTransaction()
            try {
                let rows = await db.execute(sql,id)
                await db.commit()
                return rows
            } catch (err) {
                console.log(err)
                await db.rollback()
            }
            db.release()
        } catch (err) {
            console.log(err)
        }
    }

}

module.exports.Database = new Database()

