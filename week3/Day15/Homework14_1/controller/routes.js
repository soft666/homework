const router = require('koa-router')()
const { User } = require('../models/user.js')

module.exports = (app => {
  router
  .get('/find/:id', async (ctx, next) => {
    await ctx.render("find", { 
      user : await User.find(ctx.params.id) 
    });
  })
  .get('/findAll', async (ctx, next) => {
    await ctx.render("find", { user : await User.findAll() });
  })
  .get('/findUsername/:username', async (ctx, next) => {
    await ctx.render("find", { user : await User.findByUsername(ctx.params.username) });
  })
  .get('/save', async (ctx, next) => {
    const Users = await User.find(1111)
    User.firstname = 'Ethereum Cash'
    User.lastname = 'Vitalik'
    await User.save()
    await ctx.render("find", { user : await User.findAll() });
  })
  .get('/remove/:id', async (ctx, next) => {
    const Users = await User.find(ctx.params.id)
    await User.remove()
    await ctx.render("find", { user : await User.findAll() });
  })

  app.use(router.routes());
})


