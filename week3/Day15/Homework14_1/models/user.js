const { Database } = require('../lib/db.js')

class User {
    async find(id) {
        try {
            const [rows] = await Database.exc_pool(`select id, firstname, lastname from user where id = ?` ,[id])
            this.id = rows[0].id
            this.firstname = rows[0].firstname
            this.lastname = rows[0].lastname
            return [rows[0]]
        } catch (err) {
            console.log(err)
            return
        }
    }

    async findAll() {
        const [rows] = await Database.exc_pool(`select id, firstname, lastname from user`,[])
        return rows
    }

    async findByUsername(username) {
        const [rows] = await Database.exc_pool(`select id, firstname, lastname from user where firstname = ?` ,[username])
        this.id = rows[0].id
        this.firstname = rows[0].firstname
        this.lastname = rows[0].lastname
        return [rows[0]]
    }

    async save() {
        if(!this.id) {
            let result = await Database.exc_pool(`INSERT INTO user (firstname, lastname) VALUES (?, ?)`, [this.firstname, this.lastname])
            this.id = result[0].insertId
            return
        } else {
            let result = await Database.exc_pool(`UPDATE user  set firstname = ?, lastname = ? where id = ?`, [this.firstname, this.lastname, this.id])
            return
        }
    }

    async remove() {
        try {
            await Database.exc_pool(`Delete from user where id = ?`,[this.id])
            return
        } catch (error) {
            return
        }
        
        
    }

}

module.exports.User = new User()