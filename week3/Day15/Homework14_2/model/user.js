function createEntity(row) {
    return {
        id: row.id,
        firstname: row.firstname,
        lastname: row.lastname
        
    }
}

module.exports = function (db) {
    return {
        async find(id) {
            const [rows] = await db.execute(`select id, firstname, lastname from user where id = ? `, [id])
            try {
                return createEntity(rows[0])
            } catch (error) {
                return rows
            }
        },
        async findUsername(username) {
            const [rows] = await db.execute(`select * from user where firstname = ? `, [username])
            try {
                return createEntity(rows[0])
            } catch (error) {
                return rows
            }
        },
        async findAll() {
            const [rows] = await db.execute(`select id,firstname, lastname from user`)
            return rows.map(createEntity)
        },
        async save(user) {
            if (!user.id) {
                const result = await db.execute(`insert into user (firstname, lastname) values (?, ?)`, [user.firstname, user.lastname])
                user.id = result[0].insertId
                return 'success'
            }
            
            await db.execute(`update user set firstname = ?, lastname = ? where id = ?`,
                            [user.firstname, user.lastname, user.id])
            return
        },
        async remove(id) {
            return db.execute(`delete from user where id = ?`,[id])
        }

    }
}
