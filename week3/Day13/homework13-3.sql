mysql> SELECT @@GLOBAL.tx_isolation, @@tx_isolation;
+-----------------------+-----------------+
| @@GLOBAL.tx_isolation | @@tx_isolation  |
+-----------------------+-----------------+
| REPEATABLE-READ       | REPEATABLE-READ |
+-----------------------+-----------------+
1 row in set (0.00 sec)


+------------------+--------------+----------------------+--------------+
| Level            | Dirty reads  | Non-repeatable reads |Phantoms reads|
+------------------+--------------+----------------------+--------------+
| READ UNCOMMITTED |       Y      |          Y           |      Y       | set session transaction isolation level READ UNCOMMITTED; 
| READ COMMITTED   |       N      |          Y           |      Y       | set session transaction isolation level READ COMMITTED; 
| REPEATABLE READ  |       N      |          N           |      Y       | set session transaction isolation level REPEATABLE READ; 
| SERIALIZABLE     |       N      |          N           |      N       | set session transaction isolation level SERIALIZABLE; 
+------------------+--------------+----------------------+--------------+




