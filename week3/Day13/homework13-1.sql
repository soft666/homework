-- การบ้าน #1
-- 1.1
select sum(price) as "Total Price" from courses 
left join enrolls on enrolls.course_id = courses.id
+-------------+
| Total Price |
+-------------+
|        2465 |
+-------------+
1 row in set (0.00 sec)
-- 1.2
select students.name as 'Student Name', sum(courses.price) as 'Price' from courses 
left join enrolls on enrolls.course_id = courses.id
left join students on students.id = enrolls.student_id
WHERE students.name IS NOT NULL
GROUP BY students.name
+--------------+-------+
| Student Name | Price |
+--------------+-------+
| Captain      |    90 |
| General      |    90 |
| Han          |   180 |
| Kyle         |    90 |
| Kylo         |    20 |
| Luke         |    90 |
| Maz          |   255 |
+--------------+-------+
7 rows in set (0.00 sec)



