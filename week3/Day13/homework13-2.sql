-- การบ้าน #2
-- 2.1
select students.name as 'Student Name', count(enrolls.student_id) as 'Courses', sum(courses.price) as 'Price' from courses 
left join enrolls on enrolls.course_id = courses.id
left join students on students.id = enrolls.student_id
WHERE students.name IS NOT NULL
GROUP BY students.name
+--------------+---------+-------+
| Student Name | Courses | Price |
+--------------+---------+-------+
| Captain      |       1 |    90 |
| General      |       1 |    90 |
| Han          |       2 |   180 |
| Kyle         |       1 |    90 |
| Kylo         |       1 |    20 |
| Luke         |       1 |    90 |
| Maz          |       3 |   255 |
+--------------+---------+-------+
7 rows in set (0.01 sec)
-- 2.1
select students.name as 'Student Name', min(courses.price) as 'Price', courses.name as 'Courses Name'  from courses 
left join enrolls on enrolls.course_id = courses.id
left join students on students.id = enrolls.student_id
WHERE students.name IS NOT NULL
GROUP BY students.name 

select s.name as 'Student', c.name as 'Courses', max(c.price) as 'Price' from enrolls e
left join students s on e.student_id = s.id
left join courses c on e.course_id = c.id
where c.price = (
    select max(c1.price)
    from enrolls e1
    left join students s1 on e1.student_id = s1.id
    left join courses c1 on e1.course_id = c1.id
    where s1.id = s.id
)
group by s.name, c.name;
+---------+-------------------------+-------+
| Student | Courses                 | Price |
+---------+-------------------------+-------+
| Captain | Writing                 |    90 |
| General | Chess                   |    90 |
| Han     | Acting                  |    90 |
| Han     | Chess                   |    90 |
| Kyle    | Tennis                  |    90 |
| Kylo    | JavaScript for Beginner |    20 |
| Luke    | Acting                  |    90 |
| Maz     | OWASP Top 10            |    75 |
+---------+-------------------------+-------+
7 rows in set (0.00 sec)




select s.name as 'Student', c.name as 'Courses', min(c.price) as 'Price' from enrolls e
left join students s on e.student_id = s.id
left join courses c on e.course_id = c.id
group by s.name, c.name;
