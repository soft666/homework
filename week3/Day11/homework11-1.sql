create table employee (
 id int auto_increment,
 firstname varchar(50),
 lastname varchar(50),
 age int,
 created_at timestamp default now(),
 primary key (id)
);
+------------+-------------+------+-----+-------------------+----------------+
| Field      | Type        | Null | Key | Default           | Extra          |
+------------+-------------+------+-----+-------------------+----------------+
| id         | int(11)     | NO   | PRI | NULL              | auto_increment |
| firstname  | varchar(50) | YES  |     | NULL              |                |
| lastname   | varchar(50) | YES  |     | NULL              |                |
| age        | int(11)     | YES  |     | NULL              |                |
| created_at | timestamp   | NO   |     | CURRENT_TIMESTAMP |                |
+------------+-------------+------+-----+-------------------+----------------+
5 rows in set (0.01 sec)

create table book (
 ISBN varchar(50),
 bookname varchar(50),
 bookprice int,
 created_at timestamp default now(),
 primary key (ISBN)
);
+------------+-------------+------+-----+-------------------+-------+
| Field      | Type        | Null | Key | Default           | Extra |
+------------+-------------+------+-----+-------------------+-------+
| ISBN       | varchar(50) | NO   | PRI |                   |       |
| bookname   | varchar(50) | YES  |     | NULL              |       |
| bookprice  | int(11)     | YES  |     | NULL              |       |
| created_at | timestamp   | NO   |     | CURRENT_TIMESTAMP |       |
+------------+-------------+------+-----+-------------------+-------+
4 rows in set (0.00 sec)

create table listsell (
 ISBN varchar(50),
 employee int,
 price int,
 sum int,
 created_at timestamp default now()
);
+--------------+-------------+------+-----+-------------------+-------+
| Field        | Type        | Null | Key | Default           | Extra |
+--------------+-------------+------+-----+-------------------+-------+
| ISBN         | varchar(50) | NO   |     |                   |       |
| employee     | int(50)     | YES  |     | NULL              |       |
| price        | int(11)     | YES  |     | NULL              |       |
| sum          | int(11)     | YES  |     | NULL              |       |
| created_at   | timestamp   | NO   |     | CURRENT_TIMESTAMP |       |
+--------------+-------------+------+-----+-------------------+-------+
5 rows in set (0.01 sec)