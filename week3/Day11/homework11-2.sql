-- เพมพนกงานเขาไป 10 คน โดยทขอมลหามซกน
INSERT INTO employee (firstname, lastname, age) VALUES 
('Han', 'Solo', '24'),
('Luke', 'Skywalker', '24'),
('Kylo', 'Ren', '23'),
('General', 'Hux', '22'),
('Captain', 'Phasma', '21'),
('Maz', 'Kanata', '14'),
('Kyle', 'Katarn', '15'),
('Obi', 'Wan', '22'),
('Anakin', 'Solo', '55'),
('Jacen', 'Solo', '42');

+----+-----------+-----------+------+---------------------+
| id | firstname | lastname  | age  | created_at          |
+----+-----------+-----------+------+---------------------+
|  1 | Han       | Solo      |   24 | 2018-01-08 14:22:20 |
|  2 | Luke      | Skywalker |   24 | 2018-01-08 14:22:20 |
|  3 | Kylo      | Ren       |   23 | 2018-01-08 14:22:20 |
|  4 | General   | Hux       |   22 | 2018-01-08 14:22:20 |
|  5 | Captain   | Phasma    |   21 | 2018-01-08 14:22:20 |
|  6 | Maz       | Kanata    |   14 | 2018-01-08 14:22:20 |
|  7 | Kyle      | Katarn    |   15 | 2018-01-08 14:22:20 |
|  8 | Obi       | Wan       |   22 | 2018-01-08 14:22:20 |
|  9 | Anakin    | Solo      |   55 | 2018-01-08 14:22:20 |
| 10 | Jacen     | Solo      |   42 | 2018-01-08 14:22:20 |
+----+-----------+-----------+------+---------------------+
10 rows in set (0.00 sec)

-- พนกงานคนท 5 ลาออก ลบขอมลคนท 5 ออก
delete from employee where id = 5
+----+-----------+-----------+------+---------------------+
| id | firstname | lastname  | age  | created_at          |
+----+-----------+-----------+------+---------------------+
|  1 | Han       | Solo      |   24 | 2018-01-08 14:22:20 |
|  2 | Luke      | Skywalker |   24 | 2018-01-08 14:22:20 |
|  3 | Kylo      | Ren       |   23 | 2018-01-08 14:22:20 |
|  4 | General   | Hux       |   22 | 2018-01-08 14:22:20 |
|  6 | Maz       | Kanata    |   14 | 2018-01-08 14:22:20 |
|  7 | Kyle      | Katarn    |   15 | 2018-01-08 14:22:20 |
|  8 | Obi       | Wan       |   22 | 2018-01-08 14:22:20 |
|  9 | Anakin    | Solo      |   55 | 2018-01-08 14:22:20 |
| 10 | Jacen     | Solo      |   42 | 2018-01-08 14:22:20 |
+----+-----------+-----------+------+---------------------+
9 rows in set (0.00 sec)

-- เจาของรานอยากเกบทอยพนกงานเพม ใสทอยใหพนกงานแตละคน คนไหนไมรใหเปน
alter table employee  add column address varchar(255);
+----+-----------+-----------+------+---------------------+---------+
| id | firstname | lastname  | age  | created_at          | address |
+----+-----------+-----------+------+---------------------+---------+
|  1 | Han       | Solo      |   24 | 2018-01-08 14:22:20 | NULL    |
|  2 | Luke      | Skywalker |   24 | 2018-01-08 14:22:20 | NULL    |
|  3 | Kylo      | Ren       |   23 | 2018-01-08 14:22:20 | NULL    |
|  4 | General   | Hux       |   22 | 2018-01-08 14:22:20 | NULL    |
|  6 | Maz       | Kanata    |   14 | 2018-01-08 14:22:20 | NULL    |
|  7 | Kyle      | Katarn    |   15 | 2018-01-08 14:22:20 | NULL    |
|  8 | Obi       | Wan       |   22 | 2018-01-08 14:22:20 | NULL    |
|  9 | Anakin    | Solo      |   55 | 2018-01-08 14:22:20 | NULL    |
| 10 | Jacen     | Solo      |   42 | 2018-01-08 14:22:20 | NULL    |
+----+-----------+-----------+------+---------------------+---------+
9 rows in set (0.00 sec)
update employee set address = 'Loei' where id = 1;
update employee set address = 'Loei' where id = 3;
update employee set address = 'Samut sakhon' where id = 2;
update employee set address = 'Lampang' where id = 8;
update employee set address = 'Trat' where id = 7;
select * from employee;
+----+-----------+-----------+------+---------------------+--------------+
| id | firstname | lastname  | age  | created_at          | address      |
+----+-----------+-----------+------+---------------------+--------------+
|  1 | Han       | Solo      |   24 | 2018-01-08 14:22:20 | Loei         |
|  2 | Luke      | Skywalker |   24 | 2018-01-08 14:22:20 | Samut sakhon |
|  3 | Kylo      | Ren       |   23 | 2018-01-08 14:22:20 | Loei         |
|  4 | General   | Hux       |   22 | 2018-01-08 14:22:20 | NULL         |
|  6 | Maz       | Kanata    |   14 | 2018-01-08 14:22:20 | NULL         |
|  7 | Kyle      | Katarn    |   15 | 2018-01-08 14:22:20 | Trat         |
|  8 | Obi       | Wan       |   22 | 2018-01-08 14:22:20 | Lampang      |
|  9 | Anakin    | Solo      |   55 | 2018-01-08 14:22:20 | NULL         |
| 10 | Jacen     | Solo      |   42 | 2018-01-08 14:22:20 | NULL         |
+----+-----------+-----------+------+---------------------+--------------+
9 rows in set (0.00 sec)

-- หาจนวนพนกงานทงหมดในราน
select count(*) from employee;
+----------+
| count(*) |
+----------+
|        9 |
+----------+
1 row in set (0.00 sec)

-- แสดงรายชอพนกงานทอายนอยกวา 20 ป
select * from employee where age < 20;
+----+-----------+----------+------+---------------------+---------+
| id | firstname | lastname | age  | created_at          | address |
+----+-----------+----------+------+---------------------+---------+
|  6 | Maz       | Kanata   |   14 | 2018-01-08 14:22:20 | NULL    |
|  7 | Kyle      | Katarn   |   15 | 2018-01-08 14:22:20 | NULL    |
+----+-----------+----------+------+---------------------+---------+
2 rows in set (0.00 sec)