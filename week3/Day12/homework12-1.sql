mysql> select instructors.name from instructors left join courses on courses.teach_by = instructors.id where courses.name is null order by instructors.name;
+------------------+
| name             |
+------------------+
| Alice Waters     |
| Armin Van Buuren |
| Bob Woofward     |
| Helen Mirren     |
| Martin Scorsese  |
| Ron Howard       |
| Thomas Keller    |
+------------------+
7 rows in set (0.01 sec)

mysql> select courses.name from courses left join instructors on instructors.id = courses.teach_by where instructors.name is null order by courses.name;
+-------------------------+
| name                    |
+-------------------------+
| Database System Concept |
| JavaScript for Beginner |
| OWASP Top 10            |
+-------------------------+
3 rows in set (0.00 sec)



INSERT INTO students (name) VALUES 
('Han'),
('Luke'),
('Kylo'),
('General'),
('Captain'),
('Maz'),
('Kyle'),
('Obi'),
('Anakin'),
('Jacen');



INSERT INTO enrolls (student_id,course_id) VALUES 
(1,3),
(2,2),
(3,1),
(4,3),
(5,4),
(6,5),
(7,6),
(3,7),
(6,1);

mysql> select distinct courses.name from enrolls left join courses on courses.id = enrolls.course_id;
+-------------------------+
| name                    |
+-------------------------+
| Cooking                 |
| Acting                  |
| Chess                   |
| Writing                 |
| Conservation            |
| Tennis                  |
| JavaScript for Beginner |
| OWASP Top 10            |
+-------------------------+
8 rows in set (0.00 sec)

select distinct courses.name from courses left join enrolls on courses.id = enrolls.course_id where enrolls.course_id is null
select distinct courses.name from enrolls right join courses on courses.id = enrolls.course_id where enrolls.course_id is null

mysql> select distinct courses.name from courses left join enrolls on courses.id = enrolls.course_id where enrolls.course_id is null;
+-------------------------------------+
| name                                |
+-------------------------------------+
| Building a Fashion Brand            |
| Comedy                              |
| Cooking #2                          |
| Country Music                       |
| Database System Concept             |
| Design and Architecture             |
| Dramatic Writing                    |
| Electronic Music Production         |
| Fashion Design                      |
| Film Scoring                        |
| Filmmaking                          |
| Jazz                                |
| Photography                         |
| Screenwriting                       |
| Shooting, Ball Handler, and Scoring |
| Singing                             |
| The Art of Performance              |
| Writing #2                          |
| Writing for Television              |
+-------------------------------------+
19 rows in set (0.00 sec)


-- select distinct instructors.name, courses.price from instructors left join courses on courses.teach_by = instructors.id where courses.name is not null

select distinct courses.name as 'Course Name', instructors.name as 'Instructors Name', courses.price as 'Price' from enrolls 
left join courses on courses.id = enrolls.course_id
left join instructors on courses.teach_by = instructors.id
+-------------------------+-------------------+-------+
| Course Name             | Instructors Name  | Price |
+-------------------------+-------------------+-------+
| Cooking                 | Wolfgang Puck     |    90 |
| Acting                  | Samuel L. Jackson |    90 |
| Chess                   | Garry Kasparov    |    90 |
| Writing                 | Judy Blume        |    90 |
| Conservation            | Dr. Jane Goodall  |    90 |
| Tennis                  | Serena Williams   |    90 |
| JavaScript for Beginner | NULL              |    20 |
| OWASP Top 10            | NULL              |    75 |
+-------------------------+-------------------+-------+


select distinct courses.name as 'Course Name', instructors.name as 'Instructors Name', courses.price as 'Instructors Name' from courses 
left join enrolls on courses.id = enrolls.course_id 
left join instructors on courses.teach_by = instructors.id
where enrolls.course_id is null order by courses.name
+-------------------------------------+-----------------------+------------------+
| Course Name                         | Instructors Name      | Instructors Name |
+-------------------------------------+-----------------------+------------------+
| The Art of Performance              | Usher                 |               90 |
| Writing #2                          | James Patterson       |               90 |
| Building a Fashion Brand            | Diane Von Furstenberg |               90 |
| Design and Architecture             | Frank Gehry           |               90 |
| Singing                             | Christina Aguilera    |               90 |
| Jazz                                | Herbie Hancock        |               90 |
| Country Music                       | Reba Mcentire         |               90 |
| Fashion Design                      | Marc Jacobs           |               90 |
| Film Scoring                        | Hans Zimmer           |               90 |
| Comedy                              | Steve Martin          |               90 |
| Writing for Television              | Shonda Rhimes         |               90 |
| Filmmaking                          | Werner Herzog         |               90 |
| Dramatic Writing                    | David Mamet           |               90 |
| Screenwriting                       | Aaron Sorkin          |               90 |
| Electronic Music Production         | Deadmau5              |               90 |
| Cooking #2                          | Gordon Ramsay         |               90 |
| Shooting, Ball Handler, and Scoring | Stephen Curry         |               90 |
| Photography                         | Annie Leibovitz       |               90 |
| Database System Concept             | NULL                  |               30 |
+-------------------------------------+-----------------------+------------------+
19 rows in set (0.00 sec)

select distinct courses.name as 'Course Name', instructors.name as 'Instructors Name', courses.price as 'Instructors Name' from courses 
left join enrolls on courses.id = enrolls.course_id 
left join instructors on courses.teach_by = instructors.id
where enrolls.course_id is null AND instructors.name is not null
+-------------------------------------+-----------------------+------------------+
| Course Name                         | Instructors Name      | Instructors Name |
+-------------------------------------+-----------------------+------------------+
| The Art of Performance              | Usher                 |               90 |
| Writing #2                          | James Patterson       |               90 |
| Building a Fashion Brand            | Diane Von Furstenberg |               90 |
| Design and Architecture             | Frank Gehry           |               90 |
| Singing                             | Christina Aguilera    |               90 |
| Jazz                                | Herbie Hancock        |               90 |
| Country Music                       | Reba Mcentire         |               90 |
| Fashion Design                      | Marc Jacobs           |               90 |
| Film Scoring                        | Hans Zimmer           |               90 |
| Comedy                              | Steve Martin          |               90 |
| Writing for Television              | Shonda Rhimes         |               90 |
| Filmmaking                          | Werner Herzog         |               90 |
| Dramatic Writing                    | David Mamet           |               90 |
| Screenwriting                       | Aaron Sorkin          |               90 |
| Electronic Music Production         | Deadmau5              |               90 |
| Cooking #2                          | Gordon Ramsay         |               90 |
| Shooting, Ball Handler, and Scoring | Stephen Curry         |               90 |
| Photography                         | Annie Leibovitz       |               90 |
+-------------------------------------+-----------------------+------------------+
18 rows in set (0.00 sec)

select * from employees 
left join dept_manager on dept_manager.emp_no = employees.emp_no
left join departments on dept_manager.dept_no = departments.dept_no
limit 20

